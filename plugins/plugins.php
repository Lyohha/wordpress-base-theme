<?php

require_once 'class-tgm-plugin-activation.php';

function tgm_install_plugins(){
	$plugins = array(
		array(
			'name' => 'Advanced Custom Fields',
			'slug' => 'advanced-custom-fields',// plugin id from wordpress
			'required' => false,
		)
	);
	$config = array(
		'settings' => array(
			'page_title' => __('Install Required Plugins', 'theme-slug'),
			'menu_title' => __('Install Plugins', 'theme-slug'),
		),
	);
	tgmpa( $plugins, $config );
}

// load plugins from list and load theme
add_action('tgmpa_register', 'tgm_install_plugins');

?>