<?php

// Return url to post img
function get_post_img()
{
	//default img from template: /img/img-zat.png
	$img = get_the_post_thumbnail_url();
	if($img === false)
		$img = get_template_directory_uri() . '/img/img-zat.png';
	return $img;
}

?>