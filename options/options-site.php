<?php 

function site_options_page() {
?>
	<div class='wrap'>
	<h1>Site Options</h1>
	<form method='post' action='options.php'>
<?php
	settings_fields("site_about");
	do_settings_sections('site-options');
	submit_button();
?>
	</form>
	</div>
<?php
}

function description_form() {
	?>
    	<textarea class="code large-text" name="description_field" type="text" id="description_filed"><?php echo get_option('description'); ?></textarea>
    <?php
}

function init_site_option() {
	add_settings_section('site_about', 'About Site', null, 'site-options');
	
	add_settings_field('description_field', 'Description', 'description_form', 'site-options', 'site_about');
	
	register_setting('site_about', 'description_field');
	
}

add_action('admin_init', 'init_site_option');
?>