<?php

function register_styles() {
   // wp_enqueue_style( 'neos-normalize-style', get_template_directory_uri() . '/styles/example.css' );
	wp_enqueue_style( 'theme-style', get_stylesheet_uri(), array(), null ); // main style css
	
}

function register_scripts() {
	//wp_enqueue_script( 'theme-example-js', get_template_directory_uri() . '/js/example.js', array(), $theme_version, false );
	wp_enqueue_script( 'theme-jquery-min-js', get_template_directory_uri() . '/js/jquery.min.js', array(), $theme_version, false );
	wp_enqueue_script( 'theme-main-js', get_template_directory_uri() . '/js/script.js', array(), $theme_version, false );
	
}

function register_menus() {
	$locations = array(
		//'example' => __('Example Menu', 'theme'),
		'main' => __('Main Menu', 'theme'),
	);

	register_nav_menus( $locations );
}

function on_theme_set() {
	add_default_widgets();
}

// on thumbnails for post
add_theme_support('post-thumbnails');
// register styles
add_action('wp_enqueue_scripts', 'register_styles');
// register scripts
add_action('wp_enqueue_scripts', 'register_scripts');
// register menu
add_action('init', 'register_menus');
// event after set this theme
add_action( 'after_switch_theme', 'on_theme_set');

// yemplate functions
require get_template_directory() . '/template-parts/template-functions.php';
// widgets
require get_template_directory() . '/widgets/widgets.php';
// options
require get_template_directory() . '/options/options.php';
// records
require get_template_directory() . '/records/records.php';
// plugins
require get_template_directory() . '/plugins/plugins.php';


?>